const path = require("path");
const HtmlPlugin = require('html-webpack-plugin');
//const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    mode: 'development',
    context: path.resolve(__dirname, "./src"),
    resolve: {
        extensions: ['js', 'html', 'scss']
    },
    entry: {
        hello_world: './HelloWorldApp/index.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        //new VueLoaderPlugin(),
        /*new HtmlPlugin({
          template: './HelloWorldApp/index.html',
          chunksSortMode: 'auto'
        })*/
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                  // Creates `style` nodes from JS strings
                  'style-loader',
                  // Translates CSS into CommonJS
                  'css-loader',
                  // Compiles Sass to CSS
                  'sass-loader',
                ],
              },
              {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
              },
              {
                test: /\.html$/,
                loader: 'vue-template-loader',
                options: {
                    transformToRequire: {
                      img: 'src'
                    }
                }
              }
        ]
    }
};