import HelloWorld from './hello-world.html';

export default HelloWorld({
    data () {
      return {
        message: 'Hello world!'
      };
    }
  });